python-treetime (0.11.4-1) unstable; urgency=medium

  * New upstream version 0.11.4
  * d/control: add myself to uploaders.
  * d/control: declare compliance to standards version 4.7.0.
  * syntaxwarning.patch: fix SyntaxWarning. (Closes: #1086965)

 -- Étienne Mollier <emollier@debian.org>  Sat, 09 Nov 2024 21:48:14 +0100

python-treetime (0.11.1-1) unstable; urgency=medium

  [ Ananthu C V ]
  * Team upload
  * New upstream version 0.11.1
  * Bump Standards-Version to 4.6.2 (no changes needed)
  * Create a patch to remove warning on shell script by adding shebang

  [ Andreas Tille ]
  * Build-Depends: s/dh-python/dh-sequence-python3/ (routine-update)

 -- Ananthu C V <weepingclown@disroot.org>  Wed, 01 Nov 2023 01:53:29 +0530

python-treetime (0.9.4-1) unstable; urgency=medium

  * New upstream version

 -- Andreas Tille <tille@debian.org>  Tue, 11 Oct 2022 21:03:31 +0200

python-treetime (0.9.2-1) unstable; urgency=medium

  * New upstream version

 -- Andreas Tille <tille@debian.org>  Thu, 25 Aug 2022 17:47:36 +0200

python-treetime (0.9.1-1) unstable; urgency=medium

  * New upstream version

 -- Andreas Tille <tille@debian.org>  Sun, 17 Jul 2022 22:02:29 +0200

python-treetime (0.9.0-1) unstable; urgency=medium

  * Team upload.
  * New upstream version 0.9.0
  * Bump Standards-Version to 4.6.1 (no changes needed)

 -- Nilesh Patra <nilesh@debian.org>  Sat, 28 May 2022 13:38:36 +0530

python-treetime (0.8.6-1) unstable; urgency=medium

  * New upstream version

 -- Andreas Tille <tille@debian.org>  Sat, 19 Feb 2022 08:39:09 +0100

python-treetime (0.8.5-1) unstable; urgency=medium

  * New upstream version
  * Enhance watch file
  * Add missing build dependency on dh addon.

 -- Andreas Tille <tille@debian.org>  Mon, 24 Jan 2022 20:06:58 +0100

python-treetime (0.8.4-1) unstable; urgency=medium

  [ Andreas Tille ]
  * Fix watchfile to detect new versions on github (routine-update)

  [ Nilesh Patra ]
  * d/t/get-test-data: Add script to get test data
  * New upstream version 0.8.4
  * Standards-Version: 4.6.0 (routine-update)

 -- Nilesh Patra <nilesh@debian.org>  Wed, 06 Oct 2021 13:27:53 +0530

python-treetime (0.8.1-1) unstable; urgency=medium

  * Update changelog
  * New upstream version 0.8.1
  * Declare compliance with policy 4.5.1

 -- Nilesh Patra <npatra974@gmail.com>  Sat, 09 Jan 2021 16:16:56 +0530

python-treetime (0.8.0-1) unstable; urgency=medium

  * Team Upload.
  * Add autopkgtests
  * Multi-orig tarball for testing data
  * New upstream version 0.8.0
  * debhelper-compat 13 (routine-update)
  * Add salsa-ci file (routine-update)

 -- Nilesh Patra <npatra974@gmail.com>  Tue, 17 Nov 2020 19:42:58 +0530

python-treetime (0.7.5-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * Standards-Version: 4.5.0 (routine-update)
  * d/control: Added "Rules-Requires-Root: no"

 -- Steffen Moeller <moeller@debian.org>  Sun, 03 May 2020 20:08:48 +0200

python-treetime (0.7.1-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * Remove trailing whitespace in debian/changelog (routine-update)
  * Set field Upstream-Name in debian/copyright.
  * Adjusted d/watch to allow omission of "v" prefix

 -- Steffen Moeller <moeller@debian.org>  Sat, 18 Jan 2020 21:23:53 +0100

python-treetime (0.7.0~beta.1-1) UNRELEASED; urgency=medium

  * Team upload.
  * New upstream version
  * Standards-Version: 4.4.1 (routine-update)
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.
  * Fixed indent of autotest in patch
    https://github.com/neherlab/treetime/issues/102
  * Adjusted d/watch to prefer version without "-beta"


 -- Steffen Moeller <moeller@debian.org>  Sat, 18 Jan 2020 20:52:28 +0100

python-treetime (0.6.2-1) unstable; urgency=medium

  * New upstream version
  * debhelper-compat 12
  * Standards-Version: 4.4.0

 -- Andreas Tille <tille@debian.org>  Fri, 02 Aug 2019 19:53:16 +0200

python-treetime (0.5.3-1) unstable; urgency=medium

  * New upstream version
  * debhelper 12
  * Standards-Version: 4.3.0

 -- Andreas Tille <tille@debian.org>  Fri, 25 Jan 2019 18:21:37 +0100

python-treetime (0.5.2-2) unstable; urgency=medium

  * Re-upload due to accidental removal of the package (see #916906)

 -- Andreas Tille <tille@debian.org>  Thu, 20 Dec 2018 14:03:32 +0100

python-treetime (0.5.2-1) unstable; urgency=medium

  * Add Emma Hodcroft to copyright
    Closes: #909784
  * New upstream version

 -- Andreas Tille <tille@debian.org>  Wed, 05 Dec 2018 12:00:16 +0100

python-treetime (0.5.0-1) unstable; urgency=medium

  * New upstream version
  * Drop ancient X-Python-Version field
  * Switch to Python3
  * Standards-Version: 4.2.1
  * Build-Depends: python3-biopython, python3-numpy, python3-pandas,
    python3-scipy
  * Recommends: python3-matplotlib

 -- Andreas Tille <tille@debian.org>  Thu, 27 Sep 2018 14:45:27 +0200

python-treetime (0.2.4-1) unstable; urgency=medium

  * d/watch: Point to github since upstream has started using release tags
  * New upstream version
  * Point Vcs fields to salsa.debian.org
  * Standards-Version: 4.1.4
  * debhelper 11
  * Testsuite: autopkgtest-pkg-python
  * Delete get-orig-source target

 -- Andreas Tille <tille@debian.org>  Mon, 16 Apr 2018 14:47:01 +0200

python-treetime (0.0+20170607-1) unstable; urgency=medium

  * Initial release (Closes: #864823)

 -- Andreas Tille <tille@debian.org>  Wed, 14 Jun 2017 17:20:26 +0200
